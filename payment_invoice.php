
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Payment Invoice</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
     <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>


  </head>
  <div class="container">
  <?php include( 'topnavbar1.php');?> <br>
<body>	
<form action="payment_invoice_controller.php" method="post" enctype="multipart/form-data">

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Sales Payment List</a></li>
    <li><a data-toggle="tab" href="#menu1">Purchase Payment List</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
    	<br>
      <h3 style="float: left;">Sales Payment List</h3>
      <h6 style="float: right;"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">Add New Payment</button></h6>
  
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleModalLabel">Payments</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 

            <div class="col-md-12 mb-4">
              <label for="validationTooltip02">Voucher Number *</label>
              <input type="text" class="form-control" id="voucher_number" name="voucher_number" placeholder="Voucher Number *" required>
            </div>
            <br>
            <h6>Record Payment details</h6>
            <p>Add Payment received or mode for unpaid invoices</p>
            <table class="table">
            <thead class="thead-light">
              <tr>
                
                <th scope="col">AMOUNT ($) *</th>
                <th scope="col">PAYMENT DATE *</th>
                <th scope="col">REFERENCE NUMBER *</th>
              </tr>
            </thead>
            <tbody>
              <tr>
               
            </th>
            <td><input type="text" name="amount" class="form-control" id="validationTooltip02" placeholder="Amount($)" required></td>
            <td><input type="text" name="payment_date" class="form-control" id="validationTooltip02" placeholder="Select Date" required></td>
            <td><input type="text" name="reference_number" class="form-control" id="validationTooltip02" placeholder="Reference Number" required></td>

          </tr>
        </tbody>
      </table>
      	
          <h6>Adjustment Against Pending Invoices</h6>
          <p>Balance is adjusted here against the paymemt recorded above or from account balance</p>
        <br>

      <table class="table">
        <thead class="thead-light">
          <tr>
            <th scope="col">INVOICE NUMBER</th>
            <th scope="col">NET RECEIVABLE ($)</th>
            <th scope="col">RECEIVED ($)</th>
            <th scope="col">BALANCE ($)</th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <th scope="row">INV1</th>
            <td>7,440.00</td>
            <td><input type="text" name="received"></td>
            <td>7,440.00</td>
          </tr>
          <tr>
          	<th>TOTAL</th>
          	<th>7,440.00</th>
          	<th>0.00</th>
          	<th>7,440.00</th>
          </tr>
        </tbody>
      </table>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-success" name="payment_in" id="payment_in" value="Save">
            </div>
          </div>
        </div>
      </div>   
    </div>

    <div id="menu1" class="tab-pane fade">
    	<br>
      <h3>Purchase Payment List</h3> 
    </div> 
  </div>

      <br><br><br>

      <table class="table table-bordered">
          <tbody>
            <tr>
              <td><b>Filters</td></b>
              <td><p style="float: left;"><a href="#">2020-2021</a></p>
              	<p style="float: right;"><a href="#">Clear All</a></p></td>   
            </tr>
            <tr>
              <td><b>Search & Filter</td></b>
              <td>
      <div class="row">
      	
        <div class="col-md-2 mb-2">
         	<div class="dropdown">
            <label>Financial Year -</label>
           <select name="financialyear" id="financialyear">
                    <option>2017-2018</option>
                    <option>2018-2019</option>
                    <option>2019-2020</option>
                    <option>2020-2021</option>
                    <option>2021-2022</option>
           </select>
          </div>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            
          </div>
        </div>
      
        <div class="col-md-2 mb-2">
          <div class="dropdown">
            <label>Customer -</label>
           <select name="Customer" id="Customer">
                    <option>2017-2018</option>

           </select>
          </div>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            
          </div>
        </div>
       
        <div class="col-md-2 mb-2">
          <div class="dropdown">
            <label>GSTIN -</label>
           <select name="GSTIN" id="GSTIN">
                    <option>2017-2018</option>
           </select>
          </div>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            
          </div>
        </div>
       


        <div class="col-md-2 mb-2">
          <div class="dropdown">
            <label>Payment Mode -</label>
            <select name="paymemt_mode" id="paymemt_mode">
                    <option value="" disabled>Select One</option>
                    <option>Cash</option>
                    <option>Bank</option>
                    <option>TDS</option>
                    <option>Write-off</option>
                  </select>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              
            </div>
          </div>
        </div>
    </div>

            </td>   
            </tr>  
            <tr>
              <td><b>Filter Summary</b></td>
                <td>
                	<div class="form-row">
                		<div class="col-md-4 mb-3">
                		  <p>Total Payments<br>1</p>
                    </div>
                		<div class="col-md-4 mb-3">
                		  <p>Total Amount Received($)<br>2,000.00</p>
                    </div>
                  </div>
                </td>
            </tr>
          </tbody>
        </table>

      <br>

      
 <form>
      <div class="form-row align-items-center">
          <p style="margin-top: 1%; margin-left:10px;">Select -</p>    
          <p style=" margin-left:10px;">
             <div class="btn-group btn-group-toggle" data-toggle="buttons">
                 <label class="btn btn-success active">
                      <input type="radio" name="options" id="option1" autocomplete="off" checked>20 Entries
                 </label>
                    
                 <label class="btn btn-default">
                      <input type="radio" name="options" id="option2" autocomplete="off">50 Entries
                 </label>
  
                 <label class="btn btn-default">
                      <input type="radio" name="options" id="option3" autocomplete="off">100 Entries
                 </label>
    
             </div>
  <p style="margin-top: 1%; margin-left:10px;">per page</p> 
   <h6 style="float: left; margin-left: 80%;"><button type="button" class="btn btn-success">Download all Payment as Excel</button></h6>
      </div>
 </form>
     

          <br><br>

          <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">DATE</th>
                <th scope="col">PAYMENT ID</th>
                <th scope="col">CUSTOMER</th>
                <th scope="col">GSTIN</th>
                <th scope="col">AMOUNT RECEIVED ($)</th>
                <th scope="col">PAYMENT MODE</th>
                <th scope="col">REFERENCE NO.</th>
              </tr>
            </thead>

            <tbody>
             
              <tr>
                <th scope="row">04/07/2020</th>
                <td>321GST</td>
                <td>Pk</td>
                <td>ABC123</td>
                <td>10,0000</td>
                <td>CASH</td>
                <td>10</td>
                

              </tr>
 
            </tbody>
          </table>

          <br><br>
          <br><br>

        <div class="footer">
          <center>
          	<p>previous
          		<button type="button" class="btn btn-primary">1</button>
          		next
          	</p>
          </center>
        </div>
      <br><br>
    </div>
    </form>
</body>
</html>